"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Poppy = (function () {
    function Poppy() {
        this.id = 'poppyStyles';
    }
    Poppy.prototype.init = function () {
        if (this.isNotHave) {
            var style = document.createElement('style');
            style.type = 'text/css';
            style.id = this.id;
            style.innerHTML = this.styles;
            document.getElementsByTagName('head')[0].appendChild(style);
        }
    };
    Object.defineProperty(Poppy.prototype, "isNotHave", {
        get: function () {
            return !document.getElementById(this.id);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Poppy.prototype, "styles", {
        get: function () {
            return "\n            #filter_presets_holder li a .filter__list__item_description {\n                width: 100% !important;\n                margin-top: -29px !important;\n                padding-right: 0 !important;\n                text-align: left !important;\n            }\n            #filter_presets_holder li a .filter__list__item_label {\n                width: 100% !important;\n            }\n            #filter_presets_holder .aside__list-item_selected {\n                background-color: #f0f6f9;\n            }\n        ";
        },
        enumerable: true,
        configurable: true
    });
    return Poppy;
}());
exports.Poppy = Poppy;
//# sourceMappingURL=poppy.js.map