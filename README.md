## Status

![npm (scoped)](https://img.shields.io/npm/v/@fbkl/poppy)
![npm](https://img.shields.io/npm/dm/@fbkl/poppy)
![NPM](https://img.shields.io/npm/l/@fbkl/poppy)

# sscript
Poppy for amoCrm widgets

# Install

with npm

`npm install @fbkl/poppy`

with yarn

`yarn add @fbkl/poppy`


## use

```js
import {Poppy} from '@fbkl/poppy';

new Poppy().init();
```
