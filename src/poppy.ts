export class Poppy {

    protected id: string = 'poppyStyles';

    constructor() {
    }

    public init(): void {
        if (this.isNotHave) {
            const style = document.createElement('style');
            style.type = 'text/css';
            style.id = this.id;
            style.innerHTML = this.styles;
            document.getElementsByTagName('head')[0].appendChild(style);
        }
    }

    protected get isNotHave(): boolean {
        return !document.getElementById(this.id);
    }

    protected get styles(): string {
        return `
            #filter_presets_holder li a .filter__list__item_description {
                width: 100% !important;
                margin-top: -29px !important;
                padding-right: 0 !important;
                text-align: left !important;
            }
            #filter_presets_holder li a .filter__list__item_label {
                width: 100% !important;
            }
            #filter_presets_holder .aside__list-item_selected {
                background-color: #f0f6f9;
            }
        `;
    }
}